#ifndef GridHVCommissioning_H
#define GridHVCommissioning_H

#include "Analysis.h"
#include "TVector3.h"

#include "CutsGridHVCommissioning.h"
#include "EventBase.h"

#include <TTreeReader.h>
#include <string>

class SkimSvc;

class GridHVCommissioning : public Analysis {

public:
    GridHVCommissioning();
    ~GridHVCommissioning();

    void Initialize();
    void Execute();
    void Finalize();

    void MakeUPMPlot_No1a_PodRate(int nBins, double minRate , double maxRate);
    void MakeUPMPlot_No1b_PodRateTop(int nBins, double minRate, double maxRate);
    void MakeUPMPlot_No1c_PodRateBottom(int nBins, double minRate, double maxRate);
    void MakeUPMPlot_No3a_S1RateTotal(int nBins, double minRate, double maxRate);
    void MakeUPMPlot_No3b_SPERateTotal(int nBins, double minRate, double maxRate);
    void MakeUPMPlot_No3c_SPERateGateAnodeConnections(int nBins, double minRate, double maxRate, float cutRadius_cm );
    void MakeUPMPlot_No4_SPERatePerChannel(int nBins, double minRate, double maxRate);
    void MakeUPMPlot_No5_TotalEventLogArea(int nBins, double minLogArea, double maxLogArea);
    void MakeUPMPlot_No6_S1LogArea(int nBins, double minLogArea, double maxLogArea);
    void MakeUPMPlot_No7_TotalEventLogAreaVsTime(int nBinsTime, double minTime, unsigned long period_s, int nBinsArea, double minLogArea, double maxLogArea );
    void MakeUPMPlot_No8a_LogAreaIntegralsRelToTrigger_1us(int nBins,double minLogArea, double maxLogArea);
    void MakeUPMPlot_No8f_LogAreaIntegralsRelToTrigger_10us(int nBins, double minLogArea, double maxLogArea);
    void MakeUPMPlot_No9_PulseWidth_5p_95p(int nBins, double minWidth, double maxWidth );
    void MakeUPMPlot_No10_PulseWidth_PulseLogArea(int nBinsWidth, double minWidth, double maxWidth, int nBinsLogArea, double minLogArea, double maxLogArea );

    void MakeUPMPlot_No11_TopPMTCentroidSEs(int nBinsXY, double minXY, double maxXY, std::pair<double,double> seAreaBounds_minMax, std::pair<double,double> seWidthBounds_minMax );
    std::pair<float,float> ComputeCentroidVarianceXY( std::vector<int> chIDs, std::vector<float> chPulseAreasm, bool isTopPMTArray );
    void MakeUPMPlot_No12_TopPMTCentroidVarianceSEs(int nBinsVarXY, double minVarXY, double maxVarXY, std::pair<double,double> seAreaBounds_minMax, std::pair<double,double> seWidthBounds_minMax );
    void MakeUPMPlot_No13_TopPMTCentroidAllPulses( int nBinsXY, float minXY, float maxXY);
    void MakeUPMPlot_No14_TopPMTCentroidSPEsInEvent(int nBinsXY, float minXY, float maxXY);
    void MakeUPMPlot_No15_TopPMTCentroidVarianceSPEsInEvent(int nBinsVarXY, float minVarXY, float maxVarXY );
    void MakeUPMPlot_No16_BottomPMTCentroidSPEsInEvent(int nBinsXY, float minXY, float maxXY);
    void MakeUPMPlot_No17_BottomPMTCentroidVarianceSPEsInEvent(int nBinsVarXY, float minVarXY, float maxVarXY );
    void MakeUPMPlot_No18_DriftTimes(int nBinsDT, float minDT, float maxDT );
    void MakeUPMPlot_No19_TopCentroidR2(int nBinsR2, float minR2, float maxR2, std::pair<double,double> cutAreaBounds_minMax, std::pair<double,double> cutWidthBounds_minMax  );



protected:
    CutsGridHVCommissioning* m_cutsGridHVCommissioning;
    ConfigSvc* m_conf;
    std::map<int,TVector3> m_pmtPos;
};

#endif
