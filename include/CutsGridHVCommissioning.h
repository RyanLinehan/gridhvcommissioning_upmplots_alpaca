#ifndef CutsGridHVCommissioning_H
#define CutsGridHVCommissioning_H

#include "EventBase.h"

class CutsGridHVCommissioning {

public:
    CutsGridHVCommissioning(EventBase* eventBase);
    ~CutsGridHVCommissioning();
    bool GridHVCommissioningCutsOK();

private:
    EventBase* m_event;
};

#endif
