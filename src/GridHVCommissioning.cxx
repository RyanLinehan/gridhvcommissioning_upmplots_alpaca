#include "GridHVCommissioning.h"
#include "Analysis.h"
#include "ConfigSvc.h"
#include "CutsBase.h"
#include "CutsGridHVCommissioning.h"
#include "EventBase.h"
#include "HistSvc.h"
#include "Logger.h"
#include "SkimSvc.h"
#include "SparseSvc.h"
#include "TVector3.h"

namespace {
  std::map<int,TVector3> loadPMTposMap(const std::string pmtPosFile){
    std::map<int,TVector3> result;
    std::ifstream data(pmtPosFile);
    std::string line;
    while( std::getline(data,line) ){
      std::stringstream lineStream(line);
      std::string cell;
      std::getline(lineStream,cell,' ');
      std::string id = cell;
      std::getline(lineStream,cell,' ');
      std::string value1 = cell;
      std::getline(lineStream,cell,' ');
      std::string value2 = cell;
      std::getline(lineStream,cell,' ');
      std::string value3 = cell;
      if("Channel" != id && (id.length()>0) && '#' != id[0] ){
	int channelID = std::stoi(id);
	float xPos = std::stof(value1)/10.;
	float yPos = std::stof(value2)/10.;
	float zPos = std::stof(value3)/10.;
	result.emplace(channelID,TVector3(xPos,yPos,zPos));

	std::cout << "Channel: " << channelID << ", X,Y,Z: " << xPos << ", " << yPos << ", " << zPos << std::endl;

      }
    }
    cout << "Loaded PMT positions from " << pmtPosFile << endl;
    return result;
  }
}

// Constructor
GridHVCommissioning::GridHVCommissioning()
  : Analysis(),
    m_pmtPos(loadPMTposMap("/cvmfs/lz.opensciencegrid.org/LZap/ConditionsDataModel/PMTpositions-LZ-22Aug19.txt"))
{
    // Set up the expected event structure, include branches required for analysis.
    // List the branches required below, see full list here https://luxzeplin.gitlab.io/docs/softwaredocs/analysis/analysiswithrqs/rqlist.html
    // Load in the Single Scatters Branch
    m_event->IncludeBranch("ss");
    m_event->IncludeBranch("ms");
    m_event->IncludeBranch("pulsesTPC");
    m_event->IncludeBranch("tpcEventRQs");
    m_event->Initialize();
    ////////
    
    // Setup logging
    logging::set_program_name("GridHVCommissioning Analysis");
    
    // Setup the analysis specific cuts.
    m_cutsGridHVCommissioning = new CutsGridHVCommissioning(m_event);

    // Setup config service so it can be used to get config variables
    m_conf = ConfigSvc::Instance();
}

// Destructor
GridHVCommissioning::~GridHVCommissioning()
{
    delete m_cutsGridHVCommissioning;
}

/////////////////////////////////////////////////////////////////
// Start of main analysis code block
//
// Initialize() -  Called once before the event loop.
void GridHVCommissioning::Initialize()
{
    INFO("Initializing GridHVCommissioning Analysis");
}

// Execute() - Called once per event.
void GridHVCommissioning::Execute()
{
  std::cout << "Event: " << (*m_event->m_eventHeader)->eventID << std::endl;
  
  //Here, we have a function for each UPM plot we want to create and fill.
  MakeUPMPlot_No1a_PodRate(100,0,5000);
  MakeUPMPlot_No3a_S1RateTotal(100,0,30000);
  MakeUPMPlot_No3b_SPERateTotal(100,0,30000);
  MakeUPMPlot_No4_SPERatePerChannel(100,0,2500);
  MakeUPMPlot_No5_TotalEventLogArea(300,0,8);
  MakeUPMPlot_No6_S1LogArea(300,0,8);
  MakeUPMPlot_No7_TotalEventLogAreaVsTime(300,0,600,300,0,8);
  MakeUPMPlot_No8a_LogAreaIntegralsRelToTrigger_1us(300,0,8);
  MakeUPMPlot_No8f_LogAreaIntegralsRelToTrigger_10us(300,0,8);
  MakeUPMPlot_No9_PulseWidth_5p_95p(1000,0,10000);
  MakeUPMPlot_No10_PulseWidth_PulseLogArea(1000,0,10000,100,0,8);

  std::pair<float,float> areaBounds(90,130);
  std::pair<float,float> widthBounds(800,1500);
  MakeUPMPlot_No11_TopPMTCentroidSEs(100,-80,80,areaBounds,widthBounds);
  MakeUPMPlot_No12_TopPMTCentroidVarianceSEs(100,0,6000,areaBounds,widthBounds);
  MakeUPMPlot_No13_TopPMTCentroidAllPulses(100,-80,80);
  MakeUPMPlot_No14_TopPMTCentroidSPEsInEvent(100,-80,80);
  MakeUPMPlot_No15_TopPMTCentroidVarianceSPEsInEvent(100,0,6000);
  MakeUPMPlot_No16_BottomPMTCentroidSPEsInEvent(100,-80,80);
  MakeUPMPlot_No17_BottomPMTCentroidVarianceSPEsInEvent(100,0,6000);
  MakeUPMPlot_No18_DriftTimes(1000,0,1000000);
  MakeUPMPlot_No19_TopCentroidR2(100,0,6500,areaBounds,widthBounds);
  MakeUPMPlot_No3c_SPERateGateAnodeConnections(100,0,30000,30);
  
  
  

  // Make a variable from the contents of the data
  //  prevents having to write full ' (*event->Branch)->variableName ' structure many times in the analysis
  int numSS = (*m_event->m_singleScatter)->nSingleScatters;
  
  // if there is a single scatter in the event then plot the S1 pulse area
  if (numSS > 0) {
    m_hists->BookFillHist("SingleScatters", 300, 0., 3000., (*m_event->m_singleScatter)->s1Area_phd);
  }
}

// Finalize() - Called once after event loop.
void GridHVCommissioning::Finalize()
{
    INFO("Finalizing GridHVCommissioning Analysis");
}



//---------------------------------------------------------------//
//Compute the pod rate within an event, and fill a histogram
//with that single (event-level) pod rate
void GridHVCommissioning::MakeUPMPlot_No1a_PodRate(int nBins, double minRate , double maxRate)
{
  //Get the total number of pods and a proxy for the event window time
  int nPods = (*m_event->m_eventHeader)->nPods;
  int podSpan_ns = (*m_event->m_tpcEventRQs)->podSpan_ns;
  double rate = ((double)nPods/((double)podSpan_ns))*1e9;
  m_hists->BookFillHist("h_podRateTotal_Hz; Rate [Hz]; Events",nBins,minRate,maxRate,rate);
}

//---------------------------------------------------------------//
//Compute the top-array pod rate within an event, and fill a histo
//with that single (event-level) pod rate
void GridHVCommissioning::MakeUPMPlot_No1b_PodRateTop(int nBins, double minRate, double maxRate)
{
  //QUESTION/INSTRUCTIONS FOR CHICO
  //Given that there's not an easily discernable relationship between DataCollector and 
  //PMT array association in the LZap output RQs, trying to make this plot in ALPACA is
  //somewhat challenging. If it's possible for you to make a plot that plots a single-
  //event pod rate, summed over just the top PMT array, could you put that here?
  //(Method similar to the above function, No1a)

}

//---------------------------------------------------------------//
//Compute the bottom-array pod rate within an event, and fill a histo
//with that single (event-level) pod rate
void GridHVCommissioning::MakeUPMPlot_No1c_PodRateBottom(int nBins, double minRate, double maxRate)
{
  //QUESTION/INSTRUCTIONS FOR CHICO
  //Given that there's not an easily discernable relationship between DataCollector and 
  //PMT array association in the LZap output RQs, trying to make this plot in ALPACA is
  //somewhat challenging. If it's possible for you to make a plot that plots a single-
  //event pod rate, summed over just the bottom PMT array, could you put that here?
  //(Method similar to the above function, No1a)
}

//---------------------------------------------------------------//
//Compute the total S1 rate in this event using SPEs as designated by LZap processing
void GridHVCommissioning::MakeUPMPlot_No3a_S1RateTotal(int nBins, double minRate, double maxRate)
{
  //Loop through the pulses in this event and find the number that are SPEs
  int nS1s = 0;
  for( int iP = 0; iP < (*m_event->m_tpcPulses)->nPulses; ++iP ){
    if( (*m_event->m_tpcPulses)->s1Probability[iP] == 1 ){
      nS1s++;
    }
  }

  //Get the time for rate computation
  int podSpan_ns = (*m_event->m_tpcEventRQs)->podSpan_ns;
  
  //Compute the rate
  double rate_Hz = ((double)nS1s)/((double)podSpan_ns/1e9);
  
  //Fill a UPM histogram with the rate
  m_hists->BookFillHist("h_s1Rate_Hz; Rate [Hz]; Events", nBins, minRate, maxRate, rate_Hz);
}



//---------------------------------------------------------------//
//Compute the total SPE rate in this event using SPEs as designated by LZap processing
void GridHVCommissioning::MakeUPMPlot_No3b_SPERateTotal(int nBins, double minRate, double maxRate)
{
  //Loop through the pulses in this event and find the number that are SPEs
  int nSPEs = 0;
  for( int iP = 0; iP < (*m_event->m_tpcPulses)->nPulses; ++iP ){
    if( (*m_event->m_tpcPulses)->singlePEprobability[iP] == 1 ){
      nSPEs++;
    }
  }

  //Get the time for rate computation
  int podSpan_ns = (*m_event->m_tpcEventRQs)->podSpan_ns;
  
  //Compute the rate
  double rate_Hz = ((double)nSPEs)/((double)podSpan_ns/1e9);
  
  //Fill a UPM histogram with the rate
  m_hists->BookFillHist("h_speRate_Hz; Rate [Hz]; Events", nBins, minRate, maxRate, rate_Hz);
}



//---------------------------------------------------------------//
//Compute the SPE rate in this event using SPEs found in LZap-
//designated SPEs located in PMTs within a certain distance of 
//gate and anode cable connections.
void GridHVCommissioning::MakeUPMPlot_No3c_SPERateGateAnodeConnections(int nBins, double minRate, double maxRate, float cutRadius_cm )
{
  //Loop through the pulses in this event and find the number that are SPEs
  int nSPEs = 0;
  for( int iP = 0; iP < (*m_event->m_tpcPulses)->nPulses; ++iP ){
    if( (*m_event->m_tpcPulses)->singlePEprobability[iP] == 1 ){

      //Understand what the SPE's Centroid values are
      //std::cout << "Top centroid for SPE pulse: " << (*m_event->m_tpcPulses)->topCentroidX_cm[iP] << "," << (*m_event->m_tpcPulses)->topCentroidY_cm[iP] << std::endl;
      //std::cout << "Bottom centroid for SPE pulse: " << (*m_event->m_tpcPulses)->bottomCentroidX_cm[iP] << "," << (*m_event->m_tpcPulses)->bottomCentroidY_cm[iP] << std::endl;
      
      //Set the gate-anode region at an average of 17 degrees CCW of the +Y axis (looking down on TPC). (One is 9, one is 24)
      float xGateAnode_cm = -21.6;
      float yGateAnode_cm = 70.76; 
      
      //If the SPE's PMT is within a radius of 20 cm of the gate-anode connection, then increment the nSPEs.
      //(Have to check first to see if the PMT is within the top or bottom PMT array. The 99.9 indicates that the topCentroid X position is N/A)
      if( (*m_event->m_tpcPulses)->topCentroidX_cm[iP] > 90 ){
	float x_cm = (*m_event->m_tpcPulses)->bottomCentroidX_cm[iP];
	float y_cm = (*m_event->m_tpcPulses)->bottomCentroidY_cm[iP];
	if( pow(pow(x_cm-xGateAnode_cm,2) + pow(y_cm-yGateAnode_cm,2),0.5) < cutRadius_cm ){
	  nSPEs++;
	}
      }
      else if( (*m_event->m_tpcPulses)->bottomCentroidX_cm[iP] > 90 ){
	float x_cm = (*m_event->m_tpcPulses)->topCentroidX_cm[iP];
	float y_cm = (*m_event->m_tpcPulses)->topCentroidY_cm[iP];
	if( pow(pow(x_cm-xGateAnode_cm,2) + pow(y_cm-yGateAnode_cm,2),0.5) < cutRadius_cm ){
	  nSPEs++;
	}
      }
      else{
	std::cout << "Don't quite know how we got here. TPC SPE is neither in the top nor the bottom array." << std::endl; 
      }
    }
  }
  
  //Get the time for rate computation
  int podSpan_ns = (*m_event->m_tpcEventRQs)->podSpan_ns;
  
  //Compute the rate
  double rate_Hz = ((double)nSPEs)/((double)podSpan_ns/1e9);
  
  //Fill a UPM histogram with the rate
  m_hists->BookFillHist("h_speRateGateAnode_Hz; Rate [Hz]; Events", nBins, minRate, maxRate, rate_Hz);
}



//---------------------------------------------------------------//
//This computes the rate of SPEs as calculated for this event
//on a channel-by-channel basis. Right now, it uses the SPE
//pulse classification as per ALPACA, but if something like this is
//not available, we could also use as the numerator the summed area
// of all spe like pulses in the channel.
void GridHVCommissioning::MakeUPMPlot_No4_SPERatePerChannel(int nBins, double minRate, double maxRate)
{
  //Keep a map of channel id to number of SPEs in this event
  std::map<int,int> map_channelID_nSPEs;

  //Get the event pod span (used for normalization)
  int podSpan_ns = (*m_event->m_tpcEventRQs)->podSpan_ns;
  
  //Loop through the pulses in this event and find the number that are SPEs
  int nSPEs = 0;
  for( int iP = 0; iP < (*m_event->m_tpcPulses)->nPulses; ++iP ){
    if( (*m_event->m_tpcPulses)->singlePEprobability[iP] == 1 ){
      
      //Sanity check
      if( (*m_event->m_tpcPulses)->chID[iP].size() != 1 ){
	std::cout << "Weird. Channel ID vector for this SPE pulse is not of length one. Length is: " << (*m_event->m_tpcPulses)->chID[iP].size() << std::endl;
      }

      //Get the channel id
      int channelID = (*m_event->m_tpcPulses)->chID[iP][0];
      
      if( map_channelID_nSPEs.count(channelID) == 0 ){
	map_channelID_nSPEs.emplace(channelID,1);
      }
      else{
	map_channelID_nSPEs[channelID]++;
      }
    }
  }

  //Loop through the map, normalize to get a rate, and plot  
  for( std::map<int,int>::iterator it = map_channelID_nSPEs.begin(); it != map_channelID_nSPEs.end(); ++it ){
    double rate_Hz = (float)(it->second)/((float)podSpan_ns)*1e9;
    m_hists->BookFillHist("h_chID_speRate_Hz; Channel ID; SPE Rate [Hz]",541,0,541,nBins,minRate,maxRate,it->first,rate_Hz);
  }
}


//---------------------------------------------------------------//
//This computes the total event area from the non-pulse event RQs
void GridHVCommissioning::MakeUPMPlot_No5_TotalEventLogArea(int nBins, double minLogArea, double maxLogArea)
{
  float totalArea_phd = (*m_event->m_tpcEventRQs)->totalArea_phd;
  m_hists->BookFillHist("h_totalEventLogArea_phd; Event Log(Area [phd]); Events",nBins,minLogArea,maxLogArea,log10(totalArea_phd));
}

//---------------------------------------------------------------//
//This computes the S1 areas from the LZap RQ pulse list
void GridHVCommissioning::MakeUPMPlot_No6_S1LogArea(int nBins, double minLogArea, double maxLogArea)
{
  //Loop over pulses to search for S1s. If you don't already have an easy way of identifying S1-like pulses, we may
  //have to iterate on the if statement condition.
  for( int iP = 0; iP < (*m_event->m_tpcPulses)->nPulses; ++iP ){
    if( (*m_event->m_tpcPulses)->s1Probability[iP] == 1 ){
      float area = (*m_event->m_tpcPulses)->pulseArea_phd[iP];
      m_hists->BookFillHist("h_s1LogArea_phd; S1 Log(Area[phd]); S1s",nBins,minLogArea,maxLogArea,log10(area));
    }
  }
}

//---------------------------------------------------------------//
//This creates a TH2F of the total event area vs. timestamp. The
//timestamp is really the time in seconds since a reference time,
//which is set as every [period_s] since time=0. 
void GridHVCommissioning::MakeUPMPlot_No7_TotalEventLogAreaVsTime(int nBinsTime, double minTime, unsigned long period_s, int nBinsArea, double minLogArea, double maxLogArea )
{
  //Get the area
  float totalArea_phd = (*m_event->m_tpcEventRQs)->totalArea_phd;

  //Select a period for the X axis
  unsigned long triggerTimeStamp_s = (*m_event->m_eventHeader)->triggerTimeStamp_s;
  unsigned long triggerTimeStamp_ns = (*m_event->m_eventHeader)->triggerTimeStamp_ns;
  
  //Take the seconds part of the trigger timestamp and find the modulo with our period
  unsigned long modulo = triggerTimeStamp_s % period_s;
  unsigned long long totalTimeSinceReference_s = modulo + ((double)triggerTimeStamp_ns)/1e9;
  
  //Fill a histogram with this info
  m_hists->BookFillHist("h_secondsSinceReference_totalEventArea_phd; T-Tref [s]; Log(Event Area [phd])",nBinsTime,minTime,period_s,nBinsArea,minLogArea,maxLogArea,totalTimeSinceReference_s,log10(totalArea_phd));
}

//---------------------------------------------------------------//
//Use the non-pulse event RQs (TPC) to create area integrals over
//certain time windows. This selects an area within a 1 us window
//after the trigger.
void GridHVCommissioning::MakeUPMPlot_No8a_LogAreaIntegralsRelToTrigger_1us(int nBins,double minLogArea, double maxLogArea)
{
  float triggerRoiArea1us_phd = (*m_event->m_tpcEventRQs)->triggerRoiArea1us_phd;
  m_hists->BookFillHist("h_logAreaIntegralRelToTrigger1us; Log(Area Integral, 1us, [phd]); Events",nBins,minLogArea,maxLogArea,log10(triggerRoiArea1us_phd));
}

//  ^^
//  ||
//  ||
//Can we get these for 2, 4, 6, 8 us after trigger?
//  ||
//  ||
//  vv

//---------------------------------------------------------------//
//Use the non-pulse event RQs (TPC) to create area integrals over
//certain time windows. This selects an area within a 1 us window
//after the trigger.
void GridHVCommissioning::MakeUPMPlot_No8f_LogAreaIntegralsRelToTrigger_10us(int nBins, double minLogArea, double maxLogArea)
{
  float triggerRoiArea10us_phd = (*m_event->m_tpcEventRQs)->triggerRoiArea10us_phd;
  m_hists->BookFillHist("h_logAreaIntegralRelToTrigger10us; Log(Area Integral, 10us, [phd]); Events",nBins,minLogArea,maxLogArea,log10(triggerRoiArea10us_phd));
}


//---------------------------------------------------------------//
//Use the LZap pulse RQs to create pulse widths. Will probably
//need to do this with respect to the trigger so we select only
//on the triggered pulse. Can we do this with the UPM?
//NB: For now, find the pulse whose start
//point is closest to the trigger time (0);
void GridHVCommissioning::MakeUPMPlot_No9_PulseWidth_5p_95p(int nBins, double minWidth, double maxWidth )
{
  /*
  //Loop over pulses to find the one whose start point is closest to 
  //the trigger time. 
  int distFromTrigger = 99999999;
  int pulseID = -1;
  for( int iP = 0; iP < (*m_event->m_tpcPulses)->nPulses; ++iP ){
    if( abs((*m_event->m_tpcPulses)->areaFractionTime5_ns[iP]) < distFromTrigger ){
      distFromTrigger = abs((*m_event->m_tpcPulses)->areaFractionTime5_ns[iP]);
      pulseID = iP;
    }
  }
  */

  for( int iP = 0; iP < (*m_event->m_tpcPulses)->nPulses; ++iP ){
    int pulseID = iP;
    int aft95 = (*m_event->m_tpcPulses)->areaFractionTime95_ns[pulseID];
    int aft5 = (*m_event->m_tpcPulses)->areaFractionTime5_ns[pulseID];
    int width_ns = aft95-aft5;
    m_hists->BookFillHist("h_pulseWidth_ns; Pulse Width [ns]; Pulses",nBins,minWidth,maxWidth,width_ns);
  }
}

//---------------------------------------------------------------//
//Pulse area vs. pulse width
void GridHVCommissioning::MakeUPMPlot_No10_PulseWidth_PulseLogArea(int nBinsWidth, double minWidth, double maxWidth, int nBinsLogArea, double minLogArea, double maxLogArea )
{
  /*
  //GET WIDTH FIRST
  //Loop over pulses to find the one whose start point is closest to 
  //the trigger time. 
  int distFromTrigger = 99999999;
  int pulseID = -1;
  for( int iP = 0; iP < (*m_event->m_tpcPulses)->nPulses; ++iP ){
    if( abs((*m_event->m_tpcPulses)->areaFractionTime5_ns[iP]) < distFromTrigger ){
      distFromTrigger = abs((*m_event->m_tpcPulses)->areaFractionTime5_ns[iP]);
      pulseID = iP;
    }
  }
  */
  
  for( int iP = 0; iP < (*m_event->m_tpcPulses)->nPulses; ++iP ){
    int pulseID = iP;
    int aft95 = (*m_event->m_tpcPulses)->areaFractionTime95_ns[pulseID];
    int aft5 = (*m_event->m_tpcPulses)->areaFractionTime5_ns[pulseID];
    int width_ns = aft95-aft5;
    
    //GET AREA NEXT
    float pulseArea_phd = (*m_event->m_tpcPulses)->pulseArea_phd[pulseID];
    
    //Fill the histogram
    m_hists->BookFillHist("h_pulseWidth_pulseLogArea; Pulse Width [ns]; Log(Pulse Area [phd])",nBinsWidth,minWidth,maxWidth,nBinsLogArea,minLogArea,maxLogArea,width_ns,log10(pulseArea_phd));
  }
}

//---------------------------------------------------------------//
//Plotting the top PMT centroid of pulses. I'm wondering: should we
//use the closest pulse to the trigger, or just look at all pulses?
//(still assuming SEs only). For now, just do all.
void GridHVCommissioning::MakeUPMPlot_No11_TopPMTCentroidSEs(int nBinsXY, double minXY, double maxXY, std::pair<double,double> seAreaBounds_minMax, std::pair<double,double> seWidthBounds_minMax )
{
  for( int iP = 0; iP < (*m_event->m_tpcPulses)->nPulses; ++iP ){
    double pulseArea = (*m_event->m_tpcPulses)->pulseArea_phd[iP];
    int width_ns = (*m_event->m_tpcPulses)->areaFractionTime95_ns[iP] - (*m_event->m_tpcPulses)->areaFractionTime5_ns[iP];
    if( (pulseArea > seAreaBounds_minMax.first && pulseArea < seAreaBounds_minMax.second) &&
	(width_ns > seWidthBounds_minMax.first && width_ns < seWidthBounds_minMax.second) ){ 
      float x = (*m_event->m_tpcPulses)->topCentroidX_cm[iP];
      float y = (*m_event->m_tpcPulses)->topCentroidY_cm[iP];
      m_hists->BookFillHist("h_topPMTCentroidXY_SEs; X [cm]; Y [cm]",nBinsXY,minXY,maxXY,nBinsXY,minXY,maxXY,x,y);
    }
  }
}

std::pair<float,float> GridHVCommissioning::ComputeCentroidVarianceXY( std::vector<int> chIDs, std::vector<float> chPulseAreas, bool isTopPMTArray )
{
  float xMean = 0;
  float yMean = 0;
  float x2Mean = 0;
  float y2Mean = 0;
  int nCh = 0;
  for( int iC = 0; iC < chIDs.size(); ++iC ){
    int chID = chIDs[iC];
    float chPulseArea = chPulseAreas[iC];

    //Just make sure it's actually in the TPC
    if( chID > 540 ) continue;
    
    //Get the X and Y of the channel ID
    TVector3 xyz = m_pmtPos[chID];
    if( isTopPMTArray && xyz.z() < 0 ) continue;
    if( !isTopPMTArray && xyz.z() > 0 ) continue;

    xMean += xyz.x();
    yMean += xyz.y();
    x2Mean += (xyz.x()*xyz.x());
    y2Mean += (xyz.y()*xyz.y());
    nCh++;
  }

  x2Mean /= (double)nCh;
  y2Mean /= (double)nCh;
  xMean /= (double)nCh;
  yMean /= (double)nCh;
  float varX = x2Mean - xMean*xMean;
  float varY = y2Mean - yMean*yMean;

  std::cout << "Centroid variance for this pulse: " << varX << ", " << varY << std::endl;

  std::pair<float,float> output(varX,varY);
  return output;
}

//---------------------------------------------------------------//
//Plotting the top PMT centroid variance of pulses. I'm wondering: 
//should we use the closest pulse to the trigger, or just look at
//all pulses? (still assuming SEs only). For now, just do all.
//Actually, is there a variance that we can access? Alternatively,
//how do we get access to the PMT map to do this like in the ST?
void GridHVCommissioning::MakeUPMPlot_No12_TopPMTCentroidVarianceSEs(int nBinsVarXY, double minVarXY, double maxVarXY, std::pair<double,double> seAreaBounds_minMax, std::pair<double,double> seWidthBounds_minMax )
{
  for( int iP = 0; iP < (*m_event->m_tpcPulses)->nPulses; ++iP ){
    double pulseArea = (*m_event->m_tpcPulses)->pulseArea_phd[iP];
    int width_ns = (*m_event->m_tpcPulses)->areaFractionTime95_ns[iP] - (*m_event->m_tpcPulses)->areaFractionTime5_ns[iP];
    if( (pulseArea > seAreaBounds_minMax.first && pulseArea < seAreaBounds_minMax.second) &&
	(width_ns > seWidthBounds_minMax.first && width_ns < seWidthBounds_minMax.second) ){ 

      //Compute centroid variance
      std::pair<float,float> varianceXY = ComputeCentroidVarianceXY((*m_event->m_tpcPulses)->chID[iP],(*m_event->m_tpcPulses)->chPulseArea_phd[iP],true);
      //      std::cout << "varianceX of SE: " << varianceXY.first << ", varianceY: " << varianceXY.second << std::endl;
      m_hists->BookFillHist("h_topPMTCentroidVarianceXY_SEs; Var(X) [cm2]; Var(Y) [cm2]",nBinsVarXY,minVarXY,maxVarXY,nBinsVarXY,minVarXY,maxVarXY,varianceXY.first,varianceXY.second);
      m_hists->BookFillHist("h_topPMTCentroidVarianceX_SEs_1D; Var(X) [cm2]; SEs",nBinsVarXY,minVarXY,maxVarXY,varianceXY.first);
      m_hists->BookFillHist("h_topPMTCentroidVarianceY_SEs_1D; Var(Y) [cm2]; SEs",nBinsVarXY,minVarXY,maxVarXY,varianceXY.second);
    }
  } 
}  


//---------------------------------------------------------------//
//Plotting the centroids of all pulses on the top array.
void GridHVCommissioning::MakeUPMPlot_No13_TopPMTCentroidAllPulses( int nBinsXY, float minXY, float maxXY)
{
  for( int iP = 0; iP < (*m_event->m_tpcPulses)->nPulses; ++iP ){
    float x = (*m_event->m_tpcPulses)->topCentroidX_cm[iP];
    float y = (*m_event->m_tpcPulses)->topCentroidY_cm[iP];
    m_hists->BookFillHist("h_topPMTCentroidXY_allPulses; X [cm]; Y [cm]",nBinsXY,minXY,maxXY,nBinsXY,minXY,maxXY,x,y);
  }
}



//---------------------------------------------------------------//
//This takes all of the SPEs in the event, finds the top centroid of
//them, and makes that a single contribution to a TH2F. Used to
//understand if there is an asymmetry in the SPE positions in a 
//slightly more quantitative way than UPM plot 3c (with the gate-
//anode connection SPE rates)
void GridHVCommissioning::MakeUPMPlot_No14_TopPMTCentroidSPEsInEvent(int nBinsXY, float minXY, float maxXY)
{
  float xCentroid = 0;
  float yCentroid = 0;
  int nSPEs = 0;
  for( int iP = 0; iP < (*m_event->m_tpcPulses)->nPulses; ++iP ){
    if( (*m_event->m_tpcPulses)->singlePEprobability[iP] == 1 ){
      
      //Make sure the SPE is ACTUALLY in the top array
      if( (*m_event->m_tpcPulses)->topCentroidX_cm[iP] > 90 ) continue; 
      xCentroid += (*m_event->m_tpcPulses)->topCentroidX_cm[iP];
      yCentroid += (*m_event->m_tpcPulses)->topCentroidY_cm[iP];
      nSPEs++;
    }
  }
  
  //Finish the centroid computation and fill
  xCentroid /= (double)nSPEs;
  yCentroid /= (double)nSPEs;
  m_hists->BookFillHist("h_topPMTCentroidXY_SPEs; X [cm]; Y [cm]",nBinsXY,minXY,maxXY,nBinsXY,minXY,maxXY,xCentroid,yCentroid);
}


//---------------------------------------------------------------//
//This takes all of the SPEs in the event, computes the X and Y
//variances of their positions, and makes that a single contribution
//to a TH2F. (Top PMTs only)
void GridHVCommissioning::MakeUPMPlot_No15_TopPMTCentroidVarianceSPEsInEvent(int nBinsVarXY, float minVarXY, float maxVarXY )
{
  float xAvg = 0;
  float yAvg = 0;
  float x2Avg = 0;
  float y2Avg = 0;
  int nSPEs = 0;
  for( int iP = 0; iP < (*m_event->m_tpcPulses)->nPulses; ++iP ){
    if( (*m_event->m_tpcPulses)->singlePEprobability[iP] == 1 ){
      
      //Make sure the SPE is ACTUALLY in the top array
      if( (*m_event->m_tpcPulses)->topCentroidX_cm[iP] > 90 ) continue; 
      xAvg += (*m_event->m_tpcPulses)->topCentroidX_cm[iP];
      yAvg += (*m_event->m_tpcPulses)->topCentroidY_cm[iP];
      x2Avg += pow((*m_event->m_tpcPulses)->topCentroidX_cm[iP],2);
      y2Avg += pow((*m_event->m_tpcPulses)->topCentroidY_cm[iP],2);
      nSPEs++;
    }
  }
  
  //Case with no SPEs in top array
  if( nSPEs == 0 ) return;
  
  xAvg /= (double)(nSPEs);
  yAvg /= (double)(nSPEs);
  x2Avg /= (double)(nSPEs);
  y2Avg /= (double)(nSPEs);
  float xVar = x2Avg - xAvg*xAvg;
  float yVar = y2Avg - yAvg*yAvg;


  std::cout << "XVar top: " << xVar << ", yVarTop: " << yVar << std::endl;

  m_hists->BookFillHist("h_topPMTCentroidVarianceXY_SPEs; Var(X) [cm2]; Var(Y) [cm2]",nBinsVarXY,minVarXY,maxVarXY,nBinsVarXY,minVarXY,maxVarXY,xVar,yVar);
 }


//---------------------------------------------------------------//
//This takes all of the SPEs in the event, finds the bottom centroid of
//them, and makes that a single contribution to a TH2F. Used to
//understand if there is an asymmetry in the SPE positions in a 
//slightly more quantitative way than UPM plot 3c (with the gate-
//anode connection SPE rates)
void GridHVCommissioning::MakeUPMPlot_No16_BottomPMTCentroidSPEsInEvent(int nBinsXY, float minXY, float maxXY)
{
  float xCentroid = 0;
  float yCentroid = 0;
  int nSPEs = 0;
  std::cout << "New event bottomSPECentroid-------." << std::endl;
  for( int iP = 0; iP < (*m_event->m_tpcPulses)->nPulses; ++iP ){
    if( (*m_event->m_tpcPulses)->singlePEprobability[iP] == 1 ){
      
      //Make sure the SPE is ACTUALLY in the top array
      


      if( (*m_event->m_tpcPulses)->bottomCentroidX_cm[iP] > 90 ) continue; 

      //      std::cout << "xCentroid this PMT: " << (*m_event->m_tpcPulses)->bottomCentroidX_cm[iP] << ", yCentroid this PMT: " << (*m_event->m_tpcPulses)->bottomCentroidY_cm[iP] << std::endl;


      

      xCentroid += (*m_event->m_tpcPulses)->bottomCentroidX_cm[iP];
      yCentroid += (*m_event->m_tpcPulses)->bottomCentroidY_cm[iP];
      nSPEs++;
    }
  }
  
  //Finish the centroid computation and fill
  xCentroid /= (double)nSPEs;
  yCentroid /= (double)nSPEs;
  m_hists->BookFillHist("h_bottomPMTCentroidXY_SPEs; X [cm]; Y [cm]",nBinsXY,minXY,maxXY,nBinsXY,minXY,maxXY,xCentroid,yCentroid);
}


//---------------------------------------------------------------//
//This takes all of the SPEs in the event, computes the X and Y
//variances of their positions, and makes that a single contribution
//to a TH2F. (Bottom PMT SPEs only
void GridHVCommissioning::MakeUPMPlot_No17_BottomPMTCentroidVarianceSPEsInEvent(int nBinsVarXY, float minVarXY, float maxVarXY )
{
  float xAvg = 0;
  float yAvg = 0;
  float x2Avg = 0;
  float y2Avg = 0;
  int nSPEs = 0;
  for( int iP = 0; iP < (*m_event->m_tpcPulses)->nPulses; ++iP ){
    if( (*m_event->m_tpcPulses)->singlePEprobability[iP] == 1 ){
      
      //Make sure the SPE is ACTUALLY in the bottom array
      if( (*m_event->m_tpcPulses)->bottomCentroidX_cm[iP] > 90. ) continue; 
      xAvg += (*m_event->m_tpcPulses)->bottomCentroidX_cm[iP];
      yAvg += (*m_event->m_tpcPulses)->bottomCentroidY_cm[iP];
      x2Avg += pow((*m_event->m_tpcPulses)->bottomCentroidX_cm[iP],2);
      y2Avg += pow((*m_event->m_tpcPulses)->bottomCentroidY_cm[iP],2);
      nSPEs++;
    }
  }

  //Case with no SPEs in bottom array
  if( nSPEs == 0 ) return;

  xAvg /= (float)nSPEs;
  yAvg /= (float)nSPEs;
  x2Avg /= (float)nSPEs;
  y2Avg /= (float)nSPEs;
  float xVar = x2Avg - xAvg*xAvg;
  float yVar = y2Avg - yAvg*yAvg;

  m_hists->BookFillHist("h_bottomPMTCentroidVarianceXY_SPEs; Var(X) [cm2]; Var(Y) [cm2]",nBinsVarXY,minVarXY,maxVarXY,nBinsVarXY,minVarXY,maxVarXY,xVar,yVar);
 }

//---------------------------------------------------------------//
//This looks at the drift times of events, if they are single or
//multiple scatter
void GridHVCommissioning::MakeUPMPlot_No18_DriftTimes(int nBinsDT, float minDT, float maxDT )
{
  if( (*m_event->m_singleScatter)->nSingleScatters == 1 ){
    m_hists->BookFillHist("h_driftTimes; drift time [ns]; Events",nBinsDT,minDT,maxDT,(*m_event->m_singleScatter)->driftTime_ns);
  }
  else if( (*m_event->m_multipleScatter)->nMultipleScatters == 1 ){
    for( int iT = 0; iT < (*m_event->m_multipleScatter)->nMultipleScatters; ++iT ){
      m_hists->BookFillHist("h_driftTimes; drift time [ns]; Events",nBinsDT,minDT,maxDT,(*m_event->m_multipleScatter)->driftTime_ns[iT]);
    }
  }
}

//---------------------------------------------------------------//
//This looks at the 1D R^2 distribution of top PMT centroids
void GridHVCommissioning::MakeUPMPlot_No19_TopCentroidR2(int nBinsR2, float minR2, float maxR2, std::pair<double,double> cutAreaBounds_minMax, std::pair<double,double> cutWidthBounds_minMax  )
{
  for( int iP = 0; iP < (*m_event->m_tpcPulses)->nPulses; ++iP ){
    double pulseArea = (*m_event->m_tpcPulses)->pulseArea_phd[iP];
    int width_ns = (*m_event->m_tpcPulses)->areaFractionTime95_ns[iP] - (*m_event->m_tpcPulses)->areaFractionTime5_ns[iP];    
    if( pulseArea > cutAreaBounds_minMax.first && pulseArea < cutAreaBounds_minMax.second &&
	width_ns > cutWidthBounds_minMax.first && width_ns < cutWidthBounds_minMax.second ){
      float x = (*m_event->m_tpcPulses)->topCentroidX_cm[iP];
      float y = (*m_event->m_tpcPulses)->topCentroidY_cm[iP];
      m_hists->BookFillHist("h_topPMTCentroidR2_selectedPulses; R2 [cm2]; Pulses",nBinsR2,minR2,maxR2,x*x+y*y);
    }
  }
}
