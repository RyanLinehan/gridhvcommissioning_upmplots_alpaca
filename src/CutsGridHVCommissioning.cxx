#include "CutsGridHVCommissioning.h"
#include "ConfigSvc.h"

CutsGridHVCommissioning::CutsGridHVCommissioning(EventBase* eventBase)
{
    m_event = eventBase;
}

CutsGridHVCommissioning::~CutsGridHVCommissioning()
{
}

// Function that lists all of the common cuts for this Analysis
bool CutsGridHVCommissioning::GridHVCommissioningCutsOK()
{
    // List of common cuts for this analysis into one cut
    return true;
}
